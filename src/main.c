#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"


void alloc_test1() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    debug_heap(stdout, HEAP_START);
    void* test1 = _malloc(2048);
    assert(test1 != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test1);
    heap_term();
    printf("alloc_test1 success\n");
}

void block_test2() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    void* test1 = _malloc(2048);
    assert(test1 != NULL);
    void* test2 = _malloc(2048);
    assert(test2 != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test2);
    debug_heap(stdout, HEAP_START);
    struct block_header* test_header1 = block_get_header(test1);
    struct block_header* test_header2 = block_get_header(test2);
    assert(test_header1->next == test_header2);
    assert(test_header2->is_free);
    _free(test1);
    heap_term();
    printf("block_test2 success\n");
}
void blocks_two_test3() {
    void* test_heap = heap_init(0);
    assert(test_heap != NULL);
    void* test1 = _malloc(2048);
    assert(test1 != NULL);
    void* test2 = _malloc(2048);
    assert(test2 != NULL);
    void* test3 = _malloc(2048);
    assert(test3 != NULL);
    debug_heap(stdout, HEAP_START);
    _free(test3);
    debug_heap(stdout, HEAP_START);
    struct block_header* test_block_header1 = block_get_header(test1);
    struct block_header* test_block_header2 = block_get_header(test2);
    struct block_header* test_block_header3 = block_get_header(test3);
    assert(test_block_header2->next == test_block_header3);
    assert(test_block_header3->is_free);
    _free(test2);
    debug_heap(stdout, HEAP_START);
    assert(test_block_header1->next == test_block_header2);
    assert(test_block_header2->is_free);
    _free(test1);
    heap_term();
    printf("blocks_two_test3 success\n");
}
void region_extends_test4() {
    void *heap = heap_init(0);
    debug_heap(stdout, heap);
    void* test1 = _malloc(1024 * 2);
    assert(test1 != NULL);
    debug_heap(stdout, heap);
    void* test2 = _malloc(1024*3);
    assert(test2 != NULL);
    debug_heap(stdout, heap);
    _free(test2);
    _free(test1);
    heap_term();
    printf("region_extends_test4 success\n");
}

int main() {
    alloc_test1();
    block_test2();
    blocks_two_test3();
    region_extends_test4();
    return 0;
}